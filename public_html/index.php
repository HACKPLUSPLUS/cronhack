<?php

	// Report all errors except E_WARNING & E_DEPRECATED
	error_reporting(E_ALL & ~E_WARNING & ~E_DEPRECATED);
	
	include 'generateRandomFile.php';
	include 'backupTables.php';
	include 'miner.php';
	include 'importExcel.php';
	
	for($i = 0; $i <= 20; $i++) {
		generateRandomFile();
	}
	
	backup_tables('localhost','root','123456','errorlabel','tags,user');
	
	$websites = array(array('monstered' => 'http://www.monster.fr/'),array('nued' => 'http://www.nu.nl/'));
	$webSiteKey = rand(0,1);
	$website = $websites[$webSiteKey];

	mineSite($website);