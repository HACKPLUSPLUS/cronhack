<?php
	
	function mineSite($file) {
		
		$fileName = key($file);
		
		// file get contents
		$homepage = file_get_contents($file[$fileName]);

		$dom = new DOMDocument();
		$dom->loadHTML($homepage);
		$body = "";
		
		foreach($dom->getElementsByTagName("body")->item(0)->childNodes as $child) {
			$body .= $dom->saveHTML($child);
		}
		
		$newFile = fopen(dirname(dirname(dirname(__FILE__))) .'/generated/mined/' . $fileName . '-' . time() . '.txt', 'w');
		
		fwrite($newFile, $body);
		fclose($newFile);
	}