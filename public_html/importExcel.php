<?php
	//import excel file
	require_once "../vendor/autoload.php";
	
	$fileName = "sample.xlsx";
	$excelReader = PHPExcel_IOFactory::createReaderForFile($fileName);
	
	$objPHPExcel = $excelReader->load($fileName);
	
	$highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
	$highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
	
	$objPHPExcel->setActiveSheetIndex(0)->insertNewRowBefore($highestRow + 1, 1);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0,$highestRow+1,'Jesse');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('Nimit New.xlsx');

	error_log('Column : ' . $highestColumm . ' && Row :' . $highestRow);