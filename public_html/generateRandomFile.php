<?php
	function generateRandomFile() {
		$fileExtensionArray = array('.txt', '.jpg', '.pdf', '.doc', '.docx', '.gif', '.zip', '.cab', '.gz', '.bin', '.rar', '.sql', '.xml', '.locky', '.lock', '.json');
		$fileExtensionPos = rand(0, count($fileExtensionArray)-1);
	
		$fileNamePrefixArray = array('JWI', 'CV', '2016', '2015', 'MT', 'Composer', 'VTI', 'PRO', 'Mobile', 'VTI');
		$fileNamePrefixPos = rand(0, count($fileNamePrefixArray)-1);

		// generate random number
		$randomInt = rand();
	
		//generate random file name
		$randomFileName = md5($randomInt);

		// remove numbers and a + e from randomFileName 
		$lettersOnly = preg_replace('/[0-9a+e]/', '', $randomFileName);

		$randFileNameChunks = str_split($randomFileName, 5);
	
		$newFileName = $fileNamePrefixArray[$fileNamePrefixPos] . $lettersOnly . '_' . $randFileNameChunks[1] . $fileExtensionArray[$fileExtensionPos];
		$newFile = fopen(dirname(dirname(dirname(__FILE__))) .'/generated/' . $newFileName, 'w');
		$txt = openssl_random_pseudo_bytes($randomInt+$randomInt+$randomInt+$randomInt+$randomInt+$randomInt+$randomInt);
		$txt .= $txt;

		fwrite($newFile, $txt);
		fclose($newFile);
	
	
		// This is the touch time, we'll set it to rand hour in the past.
		$time = time() - (rand(1,300) * rand(4000, 7200));
	
		// Touch the file
		if (!touch(dirname(dirname(dirname(__FILE__))) .'/generated/' . $newFileName, $time)) {
			error_log('Whoops, something went wrong...');
		} else {
			error_log('Touched file with success');
		}
	}